import { EventEmitter } from 'events'

declare class ModuleEvents extends EventEmitter {
    declare onEvent(module: string, action: string, refer: string, handle: (...args: any[]) => void)
    declare emitEvent(module: string, action: string, refer: string, ...args: any[])
}
declare function enableGlobal(): void

declare global {
    namespace NodeJS {
        interface Global {
            events: ModuleEvents
        }
    }
}