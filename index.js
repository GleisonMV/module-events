const { EventEmitter } = require('events')

class ModuleEvents extends EventEmitter {

    onEvent(module, action, refer, handle) {
        console.log(`[OnEvent] [${module}] [${action}] => Register on ${refer}`);
        this.on(`${module}.${action}`, handle)
    }

    emitEvent(module, action, refer, ...args) {
        console.log(`[EmitEvent] [${module}] [${action}] => Call on ${refer}`);
        this.emit(`${module}.${action}`, ...args)
    }
}

module.exports = {
    ModuleEvents,
    enableGlobal() {
        global.events = new ModuleEvents();
    }
}